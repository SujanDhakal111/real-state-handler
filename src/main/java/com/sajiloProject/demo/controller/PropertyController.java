package com.sajiloProject.demo.controller;

import com.sajiloProject.demo.exception.ResourceNotFoundException;
import com.sajiloProject.demo.model.Property;
import com.sajiloProject.demo.model.User;
import com.sajiloProject.demo.repository.PropertyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v1")

public class PropertyController {

    @Autowired
    private PropertyRepository propertyRepository;

    @GetMapping("/users")
    public List<Property> getAllProperties() {
        return propertyRepository.findAll();
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Property> getPropertyById(@PathVariable(value = "id") Long propertyId)
            throws ResourceNotFoundException {
        Property property = propertyRepository.findById(propertyId)
                .orElseThrow(() -> new ResourceNotFoundException("property not found for this id :: " + propertyId));
        return ResponseEntity.ok().body(property);
    }

    @PostMapping("/users")
    public Property createProperty(@Valid @RequestBody Property property) {
        return propertyRepository.save(property);
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<Property> updateProperty(@PathVariable(value = "id") Long propertyId,
                                                   @Valid @RequestBody Property propertyDetails) throws ResourceNotFoundException {
        Property property = propertyRepository.findById(propertyId)
                .orElseThrow(() -> new ResourceNotFoundException("property not found for this id :: " + propertyId));

        property.setAddress(propertyDetails.getAddress());
        property.setCoordinates(propertyDetails.getCoordinates());
        property.setPrice(propertyDetails.getPrice());
        final Property updatedProperty = propertyRepository.save(property);
        return ResponseEntity.ok(updatedProperty);
    }

    @DeleteMapping("/users/{id}")
    public Map<String, Boolean> deleteProperty(@PathVariable(value = "id") Long propertyId)
            throws ResourceNotFoundException {
        Property property = propertyRepository.findById(propertyId)
                .orElseThrow(() -> new ResourceNotFoundException("property not found for this id :: " + propertyId));

        propertyRepository.delete(property);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }


}
