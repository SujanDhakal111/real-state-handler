package com.sajiloProject.demo.model;

import javax.persistence.*;

@Entity
@Table(name = "property")
public class Property {

    private long id;
    private String address;
    private int coordinates;
    private int price;

    public Property() {

    }
    public Property(String address, int coordinates, int price) {
        this.address = address;
        this.coordinates = coordinates;
        this.price = price;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "address", nullable = false)
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }

    @Column(name = "coordinates", nullable = false)
    public int getCoordinates() {
        return coordinates;
    }
    public void setCoordinates(int coordinates ) {
        this.coordinates = coordinates;
    }

    @Column(name = "price", nullable = false)
    public int getPrice() {
        return price;
    }
    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "User [id=" + id + ", address=" + address + ", coordinates=" + coordinates + ", price=" + price
                + "]";
    }
}
